import React from 'react';
import makeRequest from '../utils/makeRequest';
import { Loader } from '@uprise/loader';

class Playlist extends React.Component {

    state = {
        playlists: [],
        activePlayListNum: 1,
        animateTo: 0
    }

    componentDidMount() {
        makeRequest(`
        {
            user(id:"chilledcow"){
           
            playlists{
              name
              images {
                url
              }
            }
          }
        }
        `)
            .then(res => {
                this.setState({
                    playlists: res.user.playlists
                })
            })
            .catch(err => console.log(err));
    }

    changePlaylist = (scrollDirection) => {
        if((this.state.activePlayListNum === 1 && scrollDirection === 1) || (this.state.activePlayListNum === this.state.playlists.length && scrollDirection === -1)){
            return
        }
        this.setState(prevState => ({ 
            animateTo: (prevState.animateTo + (scrollDirection * 320)),
            activePlayListNum: prevState.activePlayListNum + (scrollDirection * -1)
        }))
    }

    render() {

        if (!this.state.playlists.length) {
            return <Loader />
        }

        const playerDisplay = this.state.playlists.map(playlist => {
            
            return (
                <div
                    className="playlist-box"
                    key={playlist.name}
                    style={{transform: `translateX(${this.state.animateTo}px)`, transition: 'all 400ms cubic-bezier(.09,-0.07,.96,.99)'}}
                    
                    >
                    <div className="rectangle-box" style={{ backgroundImage: `url(${playlist.images[0].url})`, margin: 0 }}></div>
                    <div className="playlist-name">
                        {playlist.name}
                    </div>
                </div>
            )
        })

        
        return (
            <div className="content">
                <div className="player">
                    <div 
                    onClick={() => this.changePlaylist(1)}
                    className={this.state.activePlayListNum == 1 ? 'left arrow-container disabled' : 'left arrow-container'}>
                        <h2>&lt;</h2>
                    </div>
                    <div className="playlist-boxes">
                        {playerDisplay}

                    </div>
                    <div
                        onClick={() => this.changePlaylist(-1)}
                        className={this.state.activePlayListNum == playerDisplay.length ? 'right arrow-container disabled' : 'right arrow-container'}>
                        <h2>&gt;</h2>
                    </div>
                </div>

            </div>
        )
    }
}

export default Playlist;