import React from 'react';
import axios from '../utils/axiosConfig';
import Overview from './Overview';
import Header from './Header';
import '../App.css';
import { Route, Redirect } from 'react-router-dom';
import Playlist from './Playlist';

class App extends React.Component {

  state = {
    activeLink: '/'
  }

  setActiveLink = (linkName) => this.setState({activeLink: linkName ? linkName : this.props.location.pathname})

  componentDidMount() {
    this.setState({activeLink: this.props.location.pathname})
    if (this.props.location.search) {
      const query = this.props.location.search;
      const [queryName, code] = query.substring(1).split('=');
      if (queryName === 'code') {
        axios.get(`/token?code=${code}`)
          .then(response => {
            if (response.data.access_token) {
              localStorage.setItem('access_token', response.data.access_token);
              localStorage.setItem('refresh_token', response.data.refresh_token);
              localStorage.setItem('token_expiry_time', Date.now() + (60 * 60 * 1000));
            }
            window.location.href = '/';
          })
          .catch(err => console.log(err));
      }
    } else {
      if (!localStorage.getItem('access_token')) {
        axios.get('/clientId')
          .then(res => {
            alert('You will be redirected to Spotify authenticate page. Please login to Spotify and click on Agree to continue.')
            const clientId = res.data;
            window.location.href = `https://accounts.spotify.com/authorize?response_type=code&client_id=${clientId}&redirect_uri=${('http://localhost:3035')}`
          })
          .catch(err => console.log(err));
      }
    }
  }


  render() {
    return (
      <div className="App">
        <Header setActiveLink={this.setActiveLink} activeLink={this.state.activeLink} />
        <div className="main-content">
          <Route exact path="/" component={Overview} />
          <Route exact path="/playlist" component={Playlist} />
        </div>
      </div>
    );

  }
}

export default App;
