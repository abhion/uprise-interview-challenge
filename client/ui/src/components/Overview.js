import React from 'react';
import { Button } from '@uprise/button';
import makeRequest from '../utils/makeRequest';
import { Loader } from '@uprise/loader';

class Overview extends React.Component {

    state = {
        displayName: '',
        imageUrl: ''
    }

    componentDidMount() {
        makeRequest(`
        {
            user(id:"chilledcow"){
            display_name
            images{
                url
            }
          }
        }
        `)
        .then(res => {
            this.setState({
                displayName: res.user.display_name, imageUrl: res.user.images[0].url
            })
        })
        .catch(err => console.log(err))
    }

    render() {
        if(!this.state.displayName){
            return <Loader />
        }
        return (
            <div className="content overview-container">
                <div className="rectangle-box" style={{backgroundImage: `url(${this.state.imageUrl})`}}>

                </div>
                <div className="overview-description">
                    <h2>{this.state.displayName}</h2>
                    <Button fullWidth={false} title="Follow" className="follow-btn"></Button>
                </div>
            </div>);

    }
}

export default Overview;