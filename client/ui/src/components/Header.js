import React from 'react';
import StyledLink from './StyledLink';
import Featured from './Featured';

function Header(props) {
    
    return (
        <header>
            <ul>
                <li onClick={() => props.setActiveLink('/')}>
                    <StyledLink className={props.activeLink === '/' ? 'active' : ''}  to="/">Overview</StyledLink>
                </li>
                <li onClick={() => props.setActiveLink('/playlist')}>
                    <StyledLink
                        className={props.activeLink === '/playlist' ? 'active' : ''}
                        
                        style={{ marginLeft: '22px' }}
                        to="/playlist">Playlist</StyledLink>
                </li>
                <li onClick={() => props.setActiveLink('/featured')}>
                    <StyledLink
                     className={props.activeLink === '/featured' ? 'active' : ''}
                        
                        onBlur={() => props.setActiveLink()}
                        style={{ marginLeft: '22px', color: '#6D6C79' }}
                        to="#">Featured</StyledLink>
                        {
                            props.activeLink === '/featured' ? <Featured /> : ''
                        }
                    
                </li>
            </ul>
        </header>
    );
}
export default Header;