import React from 'react';
import makeRequest from '../utils/makeRequest';
import { Loader } from '@uprise/loader';

class Featured extends React.Component {

    state = {
        artists: []
    }

    componentDidMount() {
        makeRequest(`
        {
            user(id:"chilledcow"){
              playlists{
                tracks{
                  track{
                    artists{
                       name
                    }
                  }
                }
              }
            }
          }
        `)
            .then(res => {
                let artists = [];
                const playlists = res.user.playlists;
                playlists.forEach(playlist => {
                    playlist.tracks.forEach(track => {
                        const artistsArr = track.track.artists;
                        artistsArr.forEach(artist => artists.push(artist.name))
                    })
                });
                 artists = [...new Set(artists)];
                this.setState({artists})
            })
            .catch(err => console.log(err))
    }

    render() {
        if(!this.state.artists.length){
            return <Loader /> 
        }
        const randomStart = Math.floor(Math.random() * (this.state.artists.length - 10));
        const randomArtists = this.state.artists.slice(randomStart, randomStart + 10);
        const artistsList = randomArtists.map(artistName => {
            return (
                <li key={artistName}>{artistName}</li>
            );
        })

        return (
            <div className="featured-content">
                <ul className="featured-list">
                    {artistsList}
                </ul>
            </div>
        )
    }
}

export default Featured;