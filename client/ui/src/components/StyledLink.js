import { Link } from 'react-router-dom';
import styled from 'styled-components';

const StyledLink = styled(Link)`
    font-weight: 500;
    color: #6D6C79;
    &:focus{
        color: #7D60FF !important;
    }
`

export default StyledLink;