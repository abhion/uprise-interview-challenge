import axios from 'axios';

const instance = axios.create({
   
    baseURL: 'http://localhost:3035/'
});

export default instance;