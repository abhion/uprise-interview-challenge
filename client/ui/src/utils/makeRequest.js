import axios from '../utils/axiosConfig';

const makeRequest = (query) => {

    return axios.post('/graphql', {
        access_token: localStorage.getItem('access_token'),
        token_expiry_time: localStorage.getItem('token_expiry_time'),
        refresh_token: localStorage.getItem('refresh_token'),
        gQuery: query
    })
        .then(response => {
            const res = response.data;
            const queryResult = res.result ? res.result.data : res.data;
            if (res.tokenRefreshed) {
                localStorage.setItem('access_token', res.newToken);
                localStorage.setItem('token_expiry_time', Date.now() + (60 * 60 * 1000));
            }
            return queryResult;
        })
        .catch(err => console.log(err));
}

export default makeRequest;