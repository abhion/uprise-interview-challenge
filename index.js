const express = require('express');
const app = express();
const request = require('request');
const cors = require('cors');
const spotify_graphql = require('spotify-graphql');
let config = require('./app/graphql/config.json');
const port = 3035;
const path = require('path');

app.use(cors());
app.use(express.json());
app.use(express.static(path.join(__dirname,"/client/ui/build")));
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/client/ui/build/index.html');
})

app.post('/graphql', (req, res) => {

    const graphQLQuery = req.body.gQuery;
    const tokenExpiryTime = req.body.token_expiry_time;

    if (Date.now() >= tokenExpiryTime) {
        const refreshToken = req.body.refresh_token;
        getNewToken(refreshToken)
            .then(response => {
                const { access_token: newToken } =  response;
                getDataFromSpotify(graphQLQuery, newToken)
                    .then(result => res.json({result, tokenRefreshed: true, newToken}))
                    .catch(err => res.json(err));
            })
            .catch(err => {
                console.log(err);
                res.json(err);
            });
    }
    else {
        getDataFromSpotify(graphQLQuery, req.body.access_token)
            .then(result => res.json(result))
            .catch(err => res.json(err));
    }

})

app.get('/clientId', (req, res) => {
    const { clientId } = config;
    res.json(clientId);
})

app.get('/token', (req, res) => {
    const code = req.query.code;
    var authOptions = {
        url: 'https://accounts.spotify.com/api/token',
        form: {
            code: code,
            redirect_uri: 'http://localhost:3035',
            grant_type: 'authorization_code'
        },
        headers: {
            'Authorization': 'Basic ' + (new Buffer(config.clientId + ':' + config.clientSecret).toString('base64'))
        },
        json: true
    };
    request.post(authOptions, function (err, code, body) {
        res.json(body);
    })
})

const getNewToken = (refreshToken) => {
    var authOptions = {
        url: 'https://accounts.spotify.com/api/token',
        headers: { 'Authorization': 'Basic ' + (new Buffer(config.clientId + ':' + config.clientSecret).toString('base64')) },
        form: {
            grant_type: 'refresh_token',
            refresh_token: refreshToken
        },
        json: true
    };
    return new Promise((resolve, reject) => {
        request.post(authOptions, function (error, response, body) {
            if (!error && response.statusCode === 200) {
                resolve(body);

            } else {
                reject(error);
            }
        });
    })

}

const getDataFromSpotify = (query, token) => {
    const spotifyConfig = { ...config, accessToken: token };
    return spotify_graphql.SpotifyGraphQLClient(spotifyConfig).query(query)
}

app.listen(port, () => console.log(`listening on port ${port}`));
